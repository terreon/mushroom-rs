# Mushroom

Mushroom is a real-time messaging library which supports communication
via WebSockets and inter-process communication via an AMQP message queuing
servers.
