use std::convert::TryInto;

use bytes::{Bytes, BytesMut, BufMut};

use crate::queue::QueueError;


pub enum Error {
    /// This error type is when receiving an actual error response from the
    /// called method.
    Response(u32, Bytes),
    /// Something went wrong when trying to queue the message. See the
    /// contained queue error for more information.
    QueueError(QueueError),
    /// The requested method does not exist.
    NoSuchMethod,
    /// The request was not accepted because it did not validate
    ValidationError,
    /// The request was not accepted because the called did not have
    /// the proper permissions.
    PermissionDenied,
    /// The `oneshot` was cancelled.
    /// FIXME can this ever happen? This is probably an implementation error
    /// and should not be leaked to the user of this library.
    Cancelled,
}

impl Error {
    pub fn data(&self) -> Option<Bytes> {
        match self {
            Error::Response(_, data) => Some(data.clone()),
            _ => None
        }
    }
    pub fn code(&self) -> i64 {
        match self {
            Error::Response(code, _) => (*code).try_into().unwrap(),
            Error::QueueError(_) => -99,
            Error::NoSuchMethod => -1,
            Error::ValidationError => -2,
            Error::PermissionDenied => -3,
            Error::Cancelled => -99,
        }
    }
}
