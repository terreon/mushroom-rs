/**
 * The maximum message id is limited by JavaScript only supporting
 * 64-bit floating point values. The largest exact integral value of
 * such floating point number is 2^53-1:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER
 */
const MAX_MESSAGE_ID: i64 = 9007199254740991;

/**
 * Error enum which is returned when pushing messages to the queue.
 * There are two errors that can happen. Either the queue is full
 * or the maximum message id is reached. The latter should normally
 * not happen unless you are queuing millions of messages per second
 * for a _very_ long time. The maximum message id is 2^53-1 and allows
 * you to queue one million messages per second for the next 285 years
 * without ever triggering this error.
 */
#[derive(Debug, PartialEq)]
pub enum QueueError {
    QueueFull,
    MaxMessageIdReached,
}

/**
 * Queue which takes care of generating unique message ids. This queue is
 * used for implementing reliable messaging as messages are stored in the
 * queue until the receiver acknowledges them.
 */
pub struct Queue<T> {
    items: Vec<(i64, T)>,
    last_message_id: i64,
    max_queue_size: usize
}

impl<T> Queue<T> {
    /// Create new queue
    pub fn new(max_queue_size: usize) -> Queue<T> {
        Queue {
            items: Vec::new(),
            last_message_id: -1,
            max_queue_size: max_queue_size
        }
    }
    /**
     * Push message to queue returning the assigned message id.
     */
    pub fn push(&mut self, message: T) -> Result<i64, QueueError> {
        if self.len() >= self.max_queue_size {
            return Err(QueueError::QueueFull)
        }
        if self.last_message_id >= MAX_MESSAGE_ID {
            return Err(QueueError::MaxMessageIdReached)
        }
        self.last_message_id += 1;
        self.items.push((self.last_message_id, message));
        Ok(self.last_message_id)
    }
    /**
     * Acknowledge all messages with a message id <= `message_id`
     * Returns the number of acknowledged messages
     */
    pub fn ack(&mut self, message_id: i64) -> usize {
        let result = self.items.binary_search_by_key(&message_id, |m| m.0);
        let index = match result {
            Ok(index) => index+1,
            Err(index) => index
        };
        self.items.splice(0..index, Vec::new()).len()
    }
    /**
     * Get iterator for currently queued messages. This is useful for
     * resending messages after a connection is reestablished.
     */
    pub fn iter(&self) -> QueueIter<T> {
        QueueIter::new(self)
    }
    /// Get length of queue.
    pub fn len(&self) -> usize {
        self.items.len()
    }
}

pub struct QueueIter<'a, T> {
    queue: &'a Queue<T>,
    index: usize,
}

impl<'a, T> QueueIter<'a, T> {
    pub fn new(queue: &'a Queue<T>) -> QueueIter<'a, T> {
        QueueIter {
            queue: queue,
            index: 0,
        }
    }
}

impl<'a, T> Iterator for QueueIter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<&'a T> {
        if self.index < self.queue.items.len() {
            let old_index = self.index;
            self.index = old_index + 1;
            Some(&self.queue.items.get(old_index).unwrap().1)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn push_and_ack() {
        let mut queue = crate::queue::Queue::new(256);
        // push 3 items into the queue
        assert_eq!(queue.push("foo"), Ok(0));
        assert_eq!(queue.len(), 1);
        assert_eq!(queue.push("bar"), Ok(1));
        assert_eq!(queue.len(), 2);
        assert_eq!(queue.push("baz"), Ok(2));
        assert_eq!(queue.len(), 3);
        // acknowledge first item
        assert_eq!(queue.ack(0), 1);
        assert_eq!(queue.len(), 2);
        // acknowledge first item a second time
        // the size should not change this time
        assert_eq!(queue.ack(0), 0);
        assert_eq!(queue.len(), 2);
        // acknowledge second item
        assert_eq!(queue.ack(1), 1);
        assert_eq!(queue.len(), 1);
        // acknowledge message id greater than last item in queue
        assert_eq!(queue.ack(3), 1);
        assert_eq!(queue.len(), 0);
    }
    #[test]
    fn iter() {
        let mut queue = crate::queue::Queue::new(64);
        assert_eq!(queue.push("foo"), Ok(0));
        assert_eq!(queue.push("bar"), Ok(1));
        assert_eq!(queue.push("baz"), Ok(2));
        let mut count = 0;
        for message in queue.iter() {
            match count {
                0 => assert_eq!(message, &"foo"),
                1 => assert_eq!(message, &"bar"),
                2 => assert_eq!(message, &"baz"),
                _ => assert!(false)
            }
            count += 1;
        }
        assert_eq!(count, 3);
        assert_eq!(queue.len(), 3);
    }
    #[test]
    fn max_queue_size() {
        let mut queue = crate::queue::Queue::new(2);
        assert_eq!(queue.push("foo"), Ok(0));
        assert_eq!(queue.push("bar"), Ok(1));
        assert_eq!(queue.push("baz"), Err(crate::queue::QueueError::QueueFull));
    }
}
