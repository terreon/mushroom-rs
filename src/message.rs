use bytes::{Bytes, BytesMut, BufMut};

use crate::error::Error;


pub struct ParseError {
    pub message: String
}

pub enum Message {
    Heartbeat { last_message_id: i64 },
    Notification { message_id: i64, method: String, data: Bytes },
    Request { message_id: i64, method: String, data: Bytes },
    Response { message_id: i64, request_message_id: i64, data: Bytes },
    Error { message_id: i64, request_message_id: i64, error: Error },
    Disconnect,
}

fn json_quote(s: &String) -> String {
    format!("\"{}\"", s.replace("\\", "\\\\").replace("\"", "\\\""))
}

impl Message {
    pub fn parse(_data: Bytes) -> Result<Message, ParseError> {
        unimplemented!();
    }
    pub fn serialize(&self) -> Bytes {
        match self {
            Message::Heartbeat { last_message_id } => {
                Bytes::from(format!("[0,{}]", last_message_id))
            }
            Message::Notification { message_id, method, data } => {
                let prelude = format!("[1,{},{},", message_id, json_quote(method));
                let mut bytes = BytesMut::with_capacity(prelude.len() + data.len() + 1);
                bytes.put(prelude);
                bytes.put(data);
                bytes.put("]");
                bytes.into()
            }
            Message::Request { message_id, method, data } => {
                let prelude = format!("[2,{},{},", message_id, json_quote(method));
                let mut bytes = BytesMut::with_capacity(prelude.len() + data.len() + 1);
                bytes.put(prelude);
                bytes.put(data);
                bytes.put("]");
                bytes.into()
            }
            Message::Response { message_id, request_message_id, data } => {
                let prelude = format!("[3,{},{},0,", message_id, request_message_id);
                let mut bytes = BytesMut::with_capacity(prelude.len() + data.len() + 1);
                bytes.put(prelude);
                bytes.put(data);
                bytes.put("]");
                bytes.into()
            }
            Message::Error { message_id, request_message_id, error } => {
                match error.data() {
                    Some(data) => {
                        let prelude = format!("[3,{},{},{}", message_id, request_message_id, error.code());
                        let mut bytes = BytesMut::with_capacity(prelude.len() + data.len() + 1);
                        bytes.put(prelude);
                        bytes.put(data);
                        bytes.put("]");
                        bytes.into()
                    },
                    None => {
                        Bytes::from(format!("[3,{},{},{}]", message_id, request_message_id, error.code()))
                    }
                }
            }
            Message::Disconnect => {
                Bytes::from("[-1]")
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use bytes::Bytes;
    use crate::message::Message;
    use crate::error::Error;
    #[test]
    fn json_quote() {
        use crate::message::json_quote;
        assert_eq!(json_quote(&"test".into()), "\"test\"");
        assert_eq!(json_quote(&"\"quoted\"".into()).as_str(), "\"\\\"quoted\\\"\"");
        assert_eq!(json_quote(&"nonsense\"\\".into()).as_str(), "\"nonsense\\\"\\\\\"");
    }
    #[test]
    fn serialize_heartbeat() {
        assert_eq!(
            Message::Heartbeat {
                last_message_id: 42
            }.serialize(),
            Bytes::from("[0,42]")
        );
    }
    #[test]
    fn serialize_notification() {
        assert_eq!(
            Message::Notification {
                message_id: 43,
                method: "hi".into(),
                data: Bytes::from("null")
            }.serialize(),
            Bytes::from("[1,43,\"hi\",null]")
        );
    }
    #[test]
    fn serialize_request() {
        assert_eq!(
            Message::Request {
                message_id: 44,
                method: "get_time".into(),
                data: Bytes::from("\"utc\"")
            }.serialize(),
            Bytes::from("[2,44,\"get_time\",\"utc\"]")
        );
    }
    #[test]
    fn serialize_response() {
        assert_eq!(
            Message::Response {
                message_id: 45,
                request_message_id: 44,
                data: Bytes::from("\"2019-08-17T17:30:00Z\"")
            }.serialize(),
            Bytes::from("[3,45,44,0,\"2019-08-17T17:30:00Z\"]"));
    }
    #[test]
    fn serialize_error_no_such_method() {
        assert_eq!(
            Message::Error {
                message_id: 46,
                request_message_id: 44,
                error: Error::NoSuchMethod
            }.serialize(),
            Bytes::from(format!("[3,46,44,-1]"))
        );
    }
    #[test]
    fn serialize_disconnect() {
        assert_eq!(
            Message::Disconnect {}.serialize(),
            Bytes::from("[-1]")
        );
    }
}
