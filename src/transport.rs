use crate::message::Message;


pub trait Transport {
    fn send(&mut self, message: Message);
}
