use std::collections::HashMap;

use bytes::Bytes;
use futures::sync::oneshot;
use futures::{
    Future,
    future::{
        IntoFuture,
        err as fut_err,
    }
};
use crate::transport::Transport;
use crate::message::Message;
use crate::queue::{Queue, QueueError};
use crate::error::Error;


/// Transport neutral message factory and mapper between requests
/// and responses. This is the heart of all RPC handling.
pub struct Engine<'a> {
    transport: &'a mut dyn Transport,
    requests: HashMap<i64, oneshot::Sender<Result<Bytes, Error>>>,
    queue: Queue<Bytes>,
}

impl<'a> Engine<'a> {
    pub fn new(transport: &'a mut dyn Transport, max_queue_size: usize) -> Engine<'a> {
        Engine {
            transport: transport,
            requests: HashMap::new(),
            queue: Queue::new(max_queue_size),
        }
    }
    pub fn notify(&mut self, method: String, data: Bytes) -> Result<i64, QueueError> {
        let message_id = self.queue.push(data.clone())?;
        self.transport.send(Message::Notification {
            message_id: message_id,
            method: method,
            data: data
        });
        Ok(message_id)
    }
    pub fn request(&mut self, method: String, data: Bytes) -> Box<dyn Future<Item=Bytes, Error=Error> + Send> {
        let queue_result = self.queue.push(data.clone());
        if let Err(queue_error) = queue_result {
            return Box::new(fut_err(Error::QueueError(queue_error)));
        }
        let message_id = queue_result.unwrap();
        let (sender, receiver) = oneshot::channel::<Result<Bytes, Error>>();
        self.requests.insert(message_id, sender);
        self.transport.send(Message::Request {
            message_id: message_id,
            method: method,
            data: data
        });
        Box::new(receiver.into_future()
            .then(|result| match result {
                Ok(response) => response,
                Err(_) => Err(Error::Cancelled),
            })
        )
    }
    fn ack(&mut self, message_id: i64) {
        self.transport.send(Message::Heartbeat { last_message_id: message_id });
    }
    pub fn handle(&mut self, message: Message) {
        match message {
            Message::Heartbeat { last_message_id } => {
                self.handle_heartbeat(last_message_id);
            }
            Message::Notification { message_id, method, data } => {
                self.ack(message_id);
                self.handle_notification(message_id, method, data);
            }
            Message::Request { message_id, method, data } => {
                self.ack(message_id);
                self.handle_request(message_id, method, data);
            }
            Message::Response { message_id, request_message_id, data } => {
                self.ack(message_id);
                self.handle_response(message_id, request_message_id, data);
            }
            Message::Error { message_id, request_message_id, error } => {
                self.ack(message_id);
                self.handle_error(message_id, request_message_id, error);
            }
            Message::Disconnect => self.handle_disconnect()
        }
    }
    pub fn handle_heartbeat(&mut self, last_message_id: i64) {
        self.queue.ack(last_message_id);
    }
    pub fn handle_notification(&self, message_id: i64, method: String, data: Bytes) {
        unimplemented!();
    }
    pub fn handle_request(&self, message_id: i64, method: String, data: Bytes) {
        unimplemented!();
    }
    pub fn handle_response(&mut self, message_id: i64, request_message_id: i64, data: Bytes) {
        match self.requests.remove(&request_message_id) {
            Some(sender) => {
                if let Err(_) = sender.send(Ok(data)) {
                    // TODO log some warning about closed channel
                }
            },
            None => {
                // TODO log some warning about unexpected response
            }
        };
    }
    pub fn handle_error(&mut self, message_id: i64, request_message_id: i64, error: Error) {
        match self.requests.remove(&request_message_id) {
            Some(sender) => {
                if let Err(_) = sender.send(Err(error)) {
                    // TODO log some warning about closed channel
                }
            },
            None => {
                // TODO log some warning about unexpected response
            }
        };
    }
    pub fn handle_disconnect(&self) {
        unimplemented!();
    }
}


#[cfg(test)]
mod tests {
    use crate::engine::{Engine, Error};
    use crate::message::Message;
    use crate::transport::Transport;
    use bytes::Bytes;
    struct MockTransport {}
    impl Transport for MockTransport {
        fn send(&mut self, _message: Message) {}
    }
    #[test]
    fn request_response() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        let mut transport = MockTransport {};
        let mut engine = Engine::new(&mut transport, 64);
        let fut = engine.request("hello".into(), Bytes::from("world"));
        assert_eq!(engine.queue.len(), 1);
        assert_eq!(engine.requests.len(), 1);
        engine.handle_heartbeat(0);
        assert_eq!(engine.queue.len(), 0);
        assert_eq!(engine.requests.len(), 1);
        engine.handle_response(0, 0, Bytes::from("Hi world!"));
        let response = rt.block_on(fut);
        match response {
            Ok(data) => assert_eq!(data, Bytes::from("Hi world!")),
            Err(e) => panic!(e)
        };
    }
    #[test]
    fn request_error() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        let mut transport = MockTransport {};
        let mut engine = Engine::new(&mut transport, 64);
        let fut = engine.request("hell".into(), Bytes::from("world"));
        assert_eq!(engine.queue.len(), 1);
        assert_eq!(engine.requests.len(), 1);
        engine.handle_heartbeat(0);
        assert_eq!(engine.queue.len(), 0);
        assert_eq!(engine.requests.len(), 1);
        engine.handle_error(0, 0, Error::Response(0, Bytes::from("fail")));
        let response = rt.block_on(fut);
        match response {
            Ok(_) => panic!("Unexpected response"),
            Err(Error::Response(code, data)) => {
                assert_eq!(code, 0);
                assert_eq!(data, Bytes::from("fail"));
            }
            Err(e) => panic!(e)
        };
    }
}
