use std::env;

#[derive(Debug)]
pub struct ConfigError {
    name: String,
    message: String,
}

impl ConfigError {
    pub fn new(name: &str, message: String) -> ConfigError {
        ConfigError {
            name: String::from(name),
            message: message,
        }
    }
}

pub fn env_string(name: &str) -> Result<String, ConfigError> {
    env::var(name).map_err(|_| ConfigError::new(name, String::from("missing in environment")))
}

pub fn env_string_default(name: &str, default: String) -> String {
    env::var(name).unwrap_or(default)
}

pub fn env_u16(name: &str) -> Result<u16, ConfigError> {
    let s = env_string(name)?;
    u16::from_str_radix(s.as_str(), 10)
        .map_err(|_| ConfigError::new(name, format!("invalid unsigned 16 bit integer: {}", s)))
}

pub fn env_u16_default(name: &str, default: u16) -> Result<u16, ConfigError> {
    match env::var(name) {
        Ok(value) => u16::from_str_radix(value.as_str(), 10)
                .map_err(|_| ConfigError::new(name, format!("invalid unsigned 16 bit integer: {}", value))),
        Err(_) => Ok(default)
    }
}

#[derive(Builder, Clone, Default, Debug)]
pub struct Config {
    host: String,
    port: u16,
    vhost: String,
    username: String,
    password: String
}

impl Config {
    pub fn from_env(prefix: &str) -> Result<Config, ConfigError> {
        Ok(Config {
            host: env_string(format!("{}HOST", prefix).as_str())?,
            port: env_u16_default(format!("{}PORT", prefix).as_str(), 5672)?,
            vhost: env_string(format!("{}VHOST", prefix).as_str())?,
            username: env_string_default(format!("{}USERNAME", prefix).as_str(), String::from("guest")),
            password: env_string_default(format!("{}PASSWORD", prefix).as_str(), String::from("")),
        })
    }
    pub fn addr(self: &Self) -> String {
        format!("amqp://{}:{}@{}:{}/{}",
            self.username,
            self.password,
            self.host,
            self.port,
            self.vhost
        )
    }
}
