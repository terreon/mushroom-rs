use crate::amqp::config::Config;
use lapin_futures::{Client as LapinClient, Channel};
use lapin::{BasicProperties, ConnectionProperties};
use lapin::options::{BasicPublishOptions, QueueDeclareOptions};
use lapin::types::FieldTable;
use futures::{Future};
use failure::Error;

pub struct Client {
    config: Config,
}

impl Client {
    pub fn new(config: Config) -> Client {
        Client {
            config: config
        }
    }
}


pub fn connect(addr: &str) -> impl Future<Item=Channel, Error=Error> {
    LapinClient::connect(&addr, ConnectionProperties::default()).map_err(Error::from).and_then(|client| {
    // create_channel returns a future that is resolved
    // once the channel is successfully created
        client.create_channel().map_err(Error::from)
    })
}
